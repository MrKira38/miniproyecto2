package com.example.developer.reallyinstantchat;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.ColorSpace;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener{

    private static final String TAG = "MainActivity";
    private SharedPreferences SharedPreference;

    //Layout
    private Button btnIngresar;
    private EditText txtNombre;

    //Firebase
    private FirebaseAuth auth;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreference = PreferenceManager.getDefaultSharedPreferences(this);

        btnIngresar = findViewById(R.id.btnEntrar);

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();

       txtNombre = (EditText)findViewById(R.id.txtNombre);
        txtNombre.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String tamanoString = String.valueOf(editable.length());
                if (Integer.parseInt(tamanoString)>2){
                    btnIngresar.setEnabled(true);
                    btnIngresar.setBackgroundColor(Color.WHITE);
                    btnIngresar.setTextColor(Color.rgb(36,107,169));

                }else{
                    btnIngresar.setEnabled(false);
                    btnIngresar.setBackgroundColor(Color.rgb(205,205,205));
                    btnIngresar.setTextColor(Color.BLACK
                    );


                }

            }
        });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Toast.makeText(this, "Google Play Services error.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {

    }
}
